﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sistema.Datos;
using Sistema.Entidades.Almacen;
using Sistema.Web.Models.Almacen.Ingreso;

namespace Sistema.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IngresosController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public IngresosController(DbContextSistema context)
        {
            _context = context;
        }

        // GET: api/Ingresos/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<IngresoViewModel>> Listar()
        {
            var ingreso = await _context.Ingresos
                .Include(i => i.usuario)
                .Include(i => i.persona)
                .OrderByDescending(i => i.idIngreso)
                .Take(100)
                .ToListAsync();

            return ingreso.Select(i => new IngresoViewModel
            {
                idIngreso = i.idIngreso,
                idProveedor = i.idProveedor,
                proveedor = i.persona.nombre,
                idUsuario = i.idUsuario,
                usuario = i.usuario.nombre,
                tipoComprobante = i.tipoComprobante,
                serieComprobante = i.serieComprobante,
                numComprobante = i.numComprobante,
                fechaHora = i.fechaHora,
                impuesto = i.impuesto,
                total = i.total,
                estado = i.estado
            });
        }

        // GET: api/Ingresos/ListarFiltro/texto
        [HttpGet("[action]/{texto}")]
        public async Task<IEnumerable<IngresoViewModel>> ListarFiltro([FromRoute] string texto)
        {
            var ingreso = await _context.Ingresos
                .Include(i => i.usuario)
                .Include(i => i.persona)
                .Where(i => i.numComprobante.Contains(texto))
                .OrderByDescending(i => i.idIngreso)
                .ToListAsync();

            return ingreso.Select(i => new IngresoViewModel
            {
                idIngreso = i.idIngreso,
                idProveedor = i.idProveedor,
                proveedor = i.persona.nombre,
                idUsuario = i.idUsuario,
                usuario = i.usuario.nombre,
                tipoComprobante = i.tipoComprobante,
                serieComprobante = i.serieComprobante,
                numComprobante = i.numComprobante,
                fechaHora = i.fechaHora,
                impuesto = i.impuesto,
                total = i.total,
                estado = i.estado
            });

        }

        // GET: api/Ingresos/Listar
        [HttpGet("[action]/{FechaInicio}/{FechaFin}")]
        public async Task<IEnumerable<IngresoViewModel>> ConsultaFechas([FromRoute]DateTime FechaInicio, DateTime FechaFin)
        {
            var ingreso = await _context.Ingresos
                .Include(i => i.usuario)
                .Include(i => i.persona)
                .Where(i => i.fechaHora >= FechaInicio)
                .Where(i => i.fechaHora <= FechaFin)
                .OrderByDescending(i => i.idIngreso)
                .Take(100)
                .ToListAsync();

            return ingreso.Select(i => new IngresoViewModel
            {
                idIngreso = i.idIngreso,
                idProveedor = i.idProveedor,
                proveedor = i.persona.nombre,
                idUsuario = i.idUsuario,
                usuario = i.usuario.nombre,
                tipoComprobante = i.tipoComprobante,
                serieComprobante = i.serieComprobante,
                numComprobante = i.numComprobante,
                fechaHora = i.fechaHora,
                impuesto = i.impuesto,
                total = i.total,
                estado = i.estado
            });
        }

        // GET: api/Ingresos/ListarDetalles
        [HttpGet("[action]/{idIngreso}")]
        public async Task<IEnumerable<DetalleViewModel>> ListarDetalles([FromRoute] int idIngreso)
        {
            var detalle = await _context.DetalleIngresos
                .Include(a => a.articulo)
                .Where(d => d.idIngreso == idIngreso)
                .ToListAsync();

            return detalle.Select(d => new DetalleViewModel
            {
                idArticulo = d.idArticulo,
                articulo = d.articulo.nombre,
                cantidad = d.cantidad,
                precio = d.precio,
            });

        }

        // PUT: api/Categorias/Anular/1
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Anular([FromRoute] int id)
        {

            if (id <= 0)
            {
                return BadRequest();
            }

            var ingreso = await _context.Ingresos.FirstOrDefaultAsync(c => c.idIngreso == id);

            if (ingreso == null)
            {
                return NotFound();
            }

            ingreso.estado = "Anulado";

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        // POST: api/Ingresos/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fechaHora = DateTime.Now;

            Ingreso ingreso = new Ingreso
            {
                idProveedor = model.idProveedor,
                idUsuario = model.idUsuario,
                tipoComprobante = model.tipoComprobante,
                serieComprobante = model.serieComprobante,
                numComprobante = model.numComprobante,
                fechaHora = fechaHora,
                impuesto = model.impuesto,
                total = model.total,
                estado = "Aceptado"
            };

            
            try
            {
                _context.Ingresos.Add(ingreso);
                await _context.SaveChangesAsync();

                var id = ingreso.idIngreso;
                foreach (var det in model.detalles)
                {
                    DetalleIngreso detalle = new DetalleIngreso
                    {
                        idIngreso = id,
                        idArticulo = det.idArticulo,
                        cantidad = det.cantidad,
                        precio = det.precio
                    };
                    _context.DetalleIngresos.Add(detalle);
                }
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }

            return Ok();
        }

        private bool IngresoExists(int id)
        {
            return _context.Ingresos.Any(e => e.idIngreso == id);
        }
    }
}