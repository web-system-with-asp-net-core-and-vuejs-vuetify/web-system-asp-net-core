﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sistema.Datos;
using Sistema.Entidades.Ventas;
using Sistema.Web.Models.Ventas.Venta;

namespace Sistema.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VentasController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public VentasController(DbContextSistema context)
        {
            _context = context;
        }

        // GET: api/Ventas/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<VentaViewModel>> Listar()
        {
            var venta = await _context.Ventas
                .Include(v => v.usuario)
                .Include(v => v.persona)
                .OrderByDescending(v => v.idVenta)
                .Take(100)
                .ToListAsync();

            return venta.Select(v => new VentaViewModel
            {
                idVenta = v.idVenta,
                idCliente = v.idCliente,
                cliente = v.persona.nombre,
                numDocumento = v.persona.numDocumento,
                direccion = v.persona.direccion,
                telefono = v.persona.telefono,
                email = v.persona.email,
                idUsuario = v.idUsuario,
                usuario = v.usuario.nombre,
                tipoComprobante = v.tipoComprobante,
                serieComprobante = v.serieComprobante,
                numComprobante = v.numComprobante,
                fechaHora = v.fechaHora,
                impuesto = v.impuesto,
                total = v.total,
                estado = v.estado
            });
        }

        // GET: api/Ventas/VentasMes12
        [HttpGet("[action]")]
        public async Task<IEnumerable<ConsultaViewModel>> VentasMes12()
        {
            var consulta = await _context.Ventas
                .GroupBy(v => v.fechaHora.Month)
                .Select(x => new { etiqueta = x.Key, valor = x.Sum(v => v.total)})
                .OrderByDescending(x => x.etiqueta)
                .Take(12)
                .ToListAsync();

            return consulta.Select(v => new ConsultaViewModel
            {
                etiqueta = v.etiqueta.ToString(),
                valor = v.valor,
            });
        }

        // GET: api/Ingresos/ListarFiltro/texto
        [HttpGet("[action]/{texto}")]
        public async Task<IEnumerable<VentaViewModel>> ListarFiltro([FromRoute] string texto)
        {
            var venta = await _context.Ventas
                .Include(v => v.usuario)
                .Include(v => v.persona)
                .Where(v => v.numComprobante.Contains(texto))
                .OrderByDescending(i => i.idVenta)
                .ToListAsync();

            return venta.Select(v => new VentaViewModel
            {
                idVenta = v.idVenta,
                idCliente = v.idCliente,
                cliente = v.persona.nombre,
                numDocumento = v.persona.numDocumento,
                direccion = v.persona.direccion,
                telefono = v.persona.telefono,
                email = v.persona.email,
                idUsuario = v.idUsuario,
                usuario = v.usuario.nombre,
                tipoComprobante = v.tipoComprobante,
                serieComprobante = v.serieComprobante,
                numComprobante = v.numComprobante,
                fechaHora = v.fechaHora,
                impuesto = v.impuesto,
                total = v.total,
                estado = v.estado
            });

        }

        // GET: api/Ventas/ConsultaFechas
        [HttpGet("[action]/{FechaInicio}/{FechaFin}")]
        public async Task<IEnumerable<VentaViewModel>> ConsultaFechas([FromRoute]DateTime FechaInicio, DateTime FechaFin)
        {
            var venta = await _context.Ventas
                .Include(v => v.usuario)
                .Include(v => v.persona)
                .Where(i => i.fechaHora >= FechaInicio)
                .Where(i => i.fechaHora <= FechaFin)
                .OrderByDescending(v => v.idVenta)
                .Take(100)
                .ToListAsync();

            return venta.Select(v => new VentaViewModel
            {
                idVenta = v.idVenta,
                idCliente = v.idCliente,
                cliente = v.persona.nombre,
                numDocumento = v.persona.numDocumento,
                direccion = v.persona.direccion,
                telefono = v.persona.telefono,
                email = v.persona.email,
                idUsuario = v.idUsuario,
                usuario = v.usuario.nombre,
                tipoComprobante = v.tipoComprobante,
                serieComprobante = v.serieComprobante,
                numComprobante = v.numComprobante,
                fechaHora = v.fechaHora,
                impuesto = v.impuesto,
                total = v.total,
                estado = v.estado
            });
        }

        // GET: api/Ingresos/ListarDetalles
        [HttpGet("[action]/{idVenta}")]
        public async Task<IEnumerable<DetalleViewModel>> ListarDetalles([FromRoute] int idVenta)
        {
            var detalle = await _context.DetalleVentas
                .Include(a => a.articulo)
                .Where(d => d.idVenta == idVenta)
                .ToListAsync();

            return detalle.Select(d => new DetalleViewModel
            {
                idArticulo = d.idArticulo,
                articulo = d.articulo.nombre,
                cantidad = d.cantidad,
                precio = d.precio,
                descuento = d.descuento,
            });

        }

        // PUT: api/Ventas/Anular/1
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Anular([FromRoute] int id)
        {

            if (id <= 0)
            {
                return BadRequest();
            }

            var venta = await _context.Ventas.FirstOrDefaultAsync(c => c.idVenta == id);

            if (venta == null)
            {
                return NotFound();
            }

            venta.estado = "Anulado";

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        // POST: api/Ventas/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fechaHora = DateTime.Now;

            Venta venta = new Venta
            {
                idCliente = model.idCliente,
                idUsuario = model.idUsuario,
                tipoComprobante = model.tipoComprobante,
                serieComprobante = model.serieComprobante,
                numComprobante = model.numComprobante,
                fechaHora = fechaHora,
                impuesto = model.impuesto,
                total = model.total,
                estado = "Aceptado"
            };


            try
            {
                _context.Ventas.Add(venta);
                await _context.SaveChangesAsync();

                var id = venta.idVenta;
                foreach (var det in model.detalles)
                {
                    DetalleVenta detalle = new DetalleVenta
                    {
                        idVenta = id,
                        idArticulo = det.idArticulo,
                        cantidad = det.cantidad,
                        precio = det.precio
                    };
                    _context.DetalleVentas.Add(detalle);
                }
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }

            return Ok();
        }


        private bool VentaExists(int id)
        {
            return _context.Ventas.Any(e => e.idVenta == id);
        }
    }
}