﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sistema.Datos;
using Sistema.Entidades.Ventas;
using Sistema.Web.Models.Ventas.Persona;

namespace Sistema.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonasController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public PersonasController(DbContextSistema context)
        {
            _context = context;
        }

        // GET: api/Personas/ListarClientes
        [HttpGet("[action]")]
        public async Task<IEnumerable<PersonaViewModel>> ListarClientes()
        {
            var persona = await _context.Personas.Where(p => p.tipoPersona == "Cliente").ToListAsync();
            return persona.Select(p => new PersonaViewModel
            {
                idPersona = p.idPersona,
                tipoPersona = p.tipoPersona,
                nombre = p.nombre,
                tipoDocumento = p.tipoDocumento,
                numDocumento = p.numDocumento,
                direccion = p.direccion,
                telefono = p.telefono,
                email = p.email
            });
        }

        // GET: api/Personas/ListarProveedores
        [HttpGet("[action]")]
        public async Task<IEnumerable<PersonaViewModel>> ListarProveedores()
        {
            var persona = await _context.Personas.Where(p => p.tipoPersona == "Proveedor").ToListAsync();
            return persona.Select(p => new PersonaViewModel
            {
                idPersona = p.idPersona,
                tipoPersona = p.tipoPersona,
                nombre = p.nombre,
                tipoDocumento = p.tipoDocumento,
                numDocumento = p.numDocumento,
                direccion = p.direccion,
                telefono = p.telefono,
                email = p.email
            });
        }

        // GET: api/Personas/SelectProveedores
        [HttpGet("[action]")]
        public async Task<IEnumerable<SelectViewModel>> SelectProveedores()
        {
            var persona = await _context.Personas.Where(p => p.tipoPersona == "Proveedor").ToListAsync();

            return persona.Select(p => new SelectViewModel
            {
                idPersona = p.idPersona,
                nombre = p.nombre
            });
        }

        // GET: api/Personas/SelectClientes
        [HttpGet("[action]")]
        public async Task<IEnumerable<SelectViewModel>> SelectClientes()
        {
            var persona = await _context.Personas.Where(p => p.tipoPersona == "Cliente").ToListAsync();

            return persona.Select(p => new SelectViewModel
            {
                idPersona = p.idPersona,
                nombre = p.nombre
            });
        }

        // POST: api/Personas/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var email = model.email.ToLower();
            if (await _context.Personas.AnyAsync(p => p.email == email))
            {
                return BadRequest("El email ya existe.");
            }

            Persona persona = new Persona
            {
                tipoPersona = model.tipoPersona,
                nombre = model.nombre,
                tipoDocumento = model.tipoDocumento,
                numDocumento = model.numDocumento,
                direccion = model.direccion,
                telefono = model.telefono,
                email = model.email.ToLower()
            };
            _context.Personas.Add(persona);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok();
        }

        // PUT: api/Personas/Actualizar
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idPersona <= 0)
            {
                return BadRequest();
            }

            var persona = await _context.Personas.FirstOrDefaultAsync(p => p.idPersona == model.idPersona);
            if (persona == null)
            {
                return NotFound();
            }

            persona.tipoPersona = model.tipoPersona;
            persona.nombre = model.nombre;
            persona.tipoDocumento = model.tipoDocumento;
            persona.numDocumento = model.numDocumento;
            persona.direccion = model.direccion;
            persona.telefono = model.telefono;
            persona.email = model.email.ToLower();

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest();
            }
            return Ok();
        }

        private bool PersonaExists(int id)
        {
            return _context.Personas.Any(e => e.idPersona == id);
        }
    }
}