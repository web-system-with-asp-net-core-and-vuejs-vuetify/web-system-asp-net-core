﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sistema.Datos;
using Sistema.Entidades.Almacen;
using Sistema.Web.Models.Almacen.Articulo;

namespace Sistema.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticulosController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public ArticulosController(DbContextSistema context)
        {
            _context = context;
        }

        // GET: api/Articulos/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<ArticuloViewModel>> Listar()
        {
            var articulo = await _context.Articulos.Include(a => a.categoria).ToListAsync();

            return articulo.Select(a => new ArticuloViewModel
            {
                idArticulo = a.idArticulo,
                idCategoria = a.idCategoria,
                categoria = a.categoria.nombre,
                codigo = a.codigo,
                nombre = a.nombre,
                stock = a.stock,
                precioVenta = a.precioVenta,
                descripcion = a.descripcion,
                condicion = a.condicion
            });

        }

        // GET: api/Articulos/ListarIngreso/texto
        [HttpGet("[action]/{texto}")]
        public async Task<IEnumerable<ArticuloViewModel>> ListarIngreso([FromRoute] string texto)
        {
            var articulo = await _context.Articulos.Include(a => a.categoria).
                Where(a=> a.nombre.Contains(texto)).
                Where(a=> a.condicion == true).
                ToListAsync();

            return articulo.Select(a => new ArticuloViewModel
            {
                idArticulo = a.idArticulo,
                idCategoria = a.idCategoria,
                categoria = a.categoria.nombre,
                codigo = a.codigo,
                nombre = a.nombre,
                stock = a.stock,
                precioVenta = a.precioVenta,
                descripcion = a.descripcion,
                condicion = a.condicion
            });
        }

        // GET: api/Articulos/ListarVenta/texto
        [HttpGet("[action]/{texto}")]
        public async Task<IEnumerable<ArticuloViewModel>> ListarVenta([FromRoute] string texto)
        {
            var articulo = await _context.Articulos.Include(a => a.categoria)
                .Where(a => a.nombre.Contains(texto))
                .Where(a => a.condicion == true)
                .Where(a => a.stock > 0)
                .ToListAsync();

            return articulo.Select(a => new ArticuloViewModel
            {
                idArticulo = a.idArticulo,
                idCategoria = a.idCategoria,
                categoria = a.categoria.nombre,
                codigo = a.codigo,
                nombre = a.nombre,
                stock = a.stock,
                precioVenta = a.precioVenta,
                descripcion = a.descripcion,
                condicion = a.condicion
            });
        }

        // GET: api/Articulos/Mostrar/1
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {

            var articulo = await _context.Articulos.Include(a => a.categoria).
                SingleOrDefaultAsync(a => a.idArticulo == id);

            if (articulo == null)
            {
                return NotFound();
            }

            return Ok(new ArticuloViewModel
            {
                idArticulo = articulo.idArticulo,
                idCategoria = articulo.idCategoria,
                categoria = articulo.categoria.nombre,
                codigo = articulo.codigo,
                nombre = articulo.nombre,
                descripcion = articulo.descripcion,
                stock = articulo.stock,
                precioVenta = articulo.precioVenta,
                condicion = articulo.condicion
            });
        }

        // GET: api/Articulos/BuscarCodigoIngreso/123456789
        [HttpGet("[action]/{codigo}")]
        public async Task<IActionResult> BuscarCodigoIngreso([FromRoute] string codigo)
        {

            var articulo = await _context.Articulos.Include(a => a.categoria).
                Where(a => a.condicion == true).
                SingleOrDefaultAsync(a => a.codigo == codigo);

            if (articulo == null)
            {
                return NotFound();
            }

            return Ok(new ArticuloViewModel
            {
                idArticulo = articulo.idArticulo,
                idCategoria = articulo.idCategoria,
                categoria = articulo.categoria.nombre,
                codigo = articulo.codigo,
                nombre = articulo.nombre,
                descripcion = articulo.descripcion,
                stock = articulo.stock,
                precioVenta = articulo.precioVenta,
                condicion = articulo.condicion
            });
        }

        // GET: api/Articulos/BuscarCodigoVenta/123456789
        [HttpGet("[action]/{codigo}")]
        public async Task<IActionResult> BuscarCodigoVenta([FromRoute] string codigo)
        {

            var articulo = await _context.Articulos.Include(a => a.categoria).
                Where(a => a.condicion == true).
                Where(a => a.stock > 0).
                SingleOrDefaultAsync(a => a.codigo == codigo);

            if (articulo == null)
            {
                return NotFound();
            }

            return Ok(new ArticuloViewModel
            {
                idArticulo = articulo.idArticulo,
                idCategoria = articulo.idCategoria,
                categoria = articulo.categoria.nombre,
                codigo = articulo.codigo,
                nombre = articulo.nombre,
                descripcion = articulo.descripcion,
                stock = articulo.stock,
                precioVenta = articulo.precioVenta,
                condicion = articulo.condicion
            });
        }

        // PUT: api/Articulos/Actualizar
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idArticulo <= 0)
            {
                return BadRequest();
            }

            var articulo = await _context.Articulos.FirstOrDefaultAsync(a => a.idArticulo == model.idArticulo);

            if (articulo == null)
            {
                return NotFound();
            }

            articulo.idCategoria = model.idCategoria;
            articulo.codigo = model.codigo;
            articulo.nombre = model.nombre;
            articulo.precioVenta = model.precioVenta;
            articulo.stock = model.stock;
            articulo.descripcion = model.descripcion;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        // POST: api/Articulos/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Articulo articulo = new Articulo
            {
                idCategoria = model.idCategoria,
                codigo = model.codigo,
                nombre = model.nombre,
                precioVenta = model.precioVenta,
                stock = model.stock,
                descripcion = model.descripcion,
                condicion = true
            };

            _context.Articulos.Add(articulo);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/Articulos/Desactivar/1
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Desactivar([FromRoute] int id)
        {

            if (id <= 0)
            {
                return BadRequest();
            }

            var articulo = await _context.Articulos.FirstOrDefaultAsync(a => a.idArticulo == id);

            if (articulo == null)
            {
                return NotFound();
            }

            articulo.condicion = false;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/Articulos/Activar/1
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Activar([FromRoute] int id)
        {

            if (id <= 0)
            {
                return BadRequest();
            }

            var articulo = await _context.Articulos.FirstOrDefaultAsync(a => a.idArticulo == id);

            if (articulo == null)
            {
                return NotFound();
            }

            articulo.condicion = true;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        //// DELETE: api/Articulos/5
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteArticulo([FromRoute] int id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var articulo = await _context.Articulos.FindAsync(id);
        //    if (articulo == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.Articulos.Remove(articulo);
        //    await _context.SaveChangesAsync();

        //    return Ok(articulo);
        //}

        private bool ArticuloExists(int id)
        {
            return _context.Articulos.Any(e => e.idArticulo == id);
        }
    }
}