﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Ventas.Persona
{
    public class PersonaViewModel
    {
        public int idPersona { get; set; }
        public string tipoPersona { get; set; }
        public string nombre { get; set; }
        public string tipoDocumento { get; set; }
        public string numDocumento { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public string email { get; set; }
    }
}
