﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Ventas.Venta
{
    public class VentaViewModel
    {
        public int idVenta { get; set; }
        public int idCliente { get; set; }
        public string cliente { get; set; }
        public string numDocumento { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public string email { get; set; }
        public int idUsuario { get; set; }
        public string usuario { get; set; }
        public string tipoComprobante { get; set; }
        public string serieComprobante { get; set; }
        public string numComprobante { get; set; }
        public DateTime fechaHora { get; set; }
        public decimal impuesto { get; set; }
        public decimal total { get; set; }
        public string estado { get; set; }
    }
}
