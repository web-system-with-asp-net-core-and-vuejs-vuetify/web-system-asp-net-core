﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Ventas.Venta
{
    public class CrearViewModel
    {
        //Propiedad maestro
        [Required]
        public int idCliente { get; set; }
        [Required]
        public int idUsuario { get; set; }
        [Required]
        public string tipoComprobante { get; set; }
        public string serieComprobante { get; set; }
        [Required]
        public string numComprobante { get; set; }
        [Required]
        public decimal impuesto { get; set; }
        [Required]
        public decimal total { get; set; }
        //Propiedades detalle
        public List<DetalleViewModel> detalles { get; set; }
    }
}
