﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Usuarios.Usuario
{
    public class UsuarioViewModel
    {
        public int idUsuario { get; set; }
        public int idRol { get; set; }
        public string rol { get; set; }
        public string nombre { get; set; }
        public string tipoDocumento { get; set; }
        public string numDocumento { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public string email { get; set; }
        public byte[] passwordHash { get; set; }
        public bool condicion { get; set; }
    }
}
