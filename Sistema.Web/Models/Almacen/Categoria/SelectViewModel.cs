﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Almacen.Categoria
{
    public class SelectViewModel
    {
        public int idCategoria { get; set; }
        public string nombre { get; set; }
    }
}
