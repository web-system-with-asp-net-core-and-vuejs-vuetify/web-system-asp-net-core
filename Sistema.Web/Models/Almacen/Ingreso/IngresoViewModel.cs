﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Almacen.Ingreso
{
    public class IngresoViewModel
    {
        public int idIngreso { get; set; }
        public int idProveedor { get; set; }
        public string proveedor { get; set; }
        public int idUsuario { get; set; }
        public string usuario { get; set; }
        public string tipoComprobante { get; set; }
        public string serieComprobante { get; set; }
        public string numComprobante { get; set; }
        public DateTime fechaHora { get; set; }
        public decimal impuesto { get; set; }
        public decimal total { get; set; }
        public string estado { get; set; }
    }
}
