﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Almacen.Ingreso
{
    public class DetalleViewModel
    {
        [Required]
        public int idArticulo { get; set; }
        public string articulo { get; set; }
        public int cantidad { get; set; }
        public decimal precio { get; set; }
    }
}
