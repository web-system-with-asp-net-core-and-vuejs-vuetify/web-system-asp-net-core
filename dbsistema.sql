create database dbsistema;
go
use dbsistema;

--Tabla categoría
create table categoria(
	idCategoria integer primary key identity,
	nombre varchar(50) not null unique,
	descripcion varchar(256) null,
	condicion bit default(1)
);
insert into categoria (nombre,descripcion) values ('Desktop','Son computadoras de escritorio');
select * from categoria;

--Tabla artículo
create table articulo(
	idArticulo integer primary key identity,
	idCategoria integer not null,
	codigo varchar(50) null,
	nombre varchar(100) not null unique,
	precioVenta decimal(11,2) not null,
	stock integer not null,
	descripcion varchar(256) null,
	condicion bit default(1),
	FOREIGN KEY (idCategoria) REFERENCES categoria(idCategoria)
);

insert into articulo values (1, '87767283', 'Asus', 10.0, 1000, 'Se usa', 1)

--Tabla persona
create table persona(
	idpersona integer primary key identity,
	tipoPersona varchar(20) not null,
	nombre varchar(100) not null,
	tipoDocumento varchar(20) null,
	numDocumento varchar(20) null,
	direccion varchar(70) null,
	telefono varchar(20) null,
	email varchar(50) null
);

--Tabla rol
create table rol(
	idRol integer primary key identity,
	nombre varchar(30) not null,
	descripcion varchar(100) null,
	condicion bit default(1)
);

INSERT INTO rol values ('Administrador', 'Administra', 1),
						('Almacenero', 'Almacena', 1),
						('Vendedor', 'Vende', 1)

--Tabla usuario
create table usuario(
	idUsuario integer primary key identity,
	idRol integer not null,
	nombre varchar(100) not null,
	tipoDocumento varchar(20) null,
	numDocumento varchar(20) null,
	direccion varchar(70) null,
	telefono varchar(20) null,
	email varchar(50) not null,
	passwordHash varbinary(MAX) not null,
	passwordSalt varbinary(MAX) not null,
	condicion bit default(1),
	FOREIGN KEY (idRol) REFERENCES rol (idRol)
);


--Tabla ingreso
create table ingreso(
	idIngreso integer primary key identity,
	idProveedor integer not null,
	idUsuario integer not null,
	tipoComprobante varchar(20) not null,
	serieComprobante varchar(7) null,
	numComprobante varchar (10) not null,
	fechaHora datetime not null,
	impuesto decimal (4,2) not null,
	total decimal (11,2) not null,
	estado varchar(20) not null,
	FOREIGN KEY (idProveedor) REFERENCES persona (idpersona),
	FOREIGN KEY (idUsuario) REFERENCES usuario (idUsuario)
);

INSERT INTO ingreso values (1, 1, 'FACTURA', '001', '001', '10/10/2018', 18.00, 100.00, 'Aceptado')

--Tabla detalle_ingreso
create table detalleIngreso(
	idDetalleIngreso integer primary key identity,
	idIngreso integer not null,
	idArticulo integer not null,
	cantidad integer not null,
	precio decimal(11,2) not null,
	FOREIGN KEY (idIngreso) REFERENCES ingreso (idIngreso) ON DELETE CASCADE,
	FOREIGN KEY (idArticulo) REFERENCES articulo (idArticulo)
);


--Tabla venta
create table venta(
	idVenta integer primary key identity,
	idCliente integer not null,
	idUsuario integer not null,
	tipoComprobante varchar(20) not null,
	serieComprobante varchar(7) null,
	numComprobante varchar (10) not null,
	fechaHora datetime not null,
	impuesto decimal (4,2) not null,
	total decimal (11,2) not null,
	estado varchar(20) not null,
	FOREIGN KEY (idCliente) REFERENCES persona (idpersona),
	FOREIGN KEY (idUsuario) REFERENCES usuario (idUsuario)
);

--Tabla detalle_venta
create table detalleVenta(
	idDetalleVenta integer primary key identity,
	idVenta integer not null,
	idArticulo integer not null,
	cantidad integer not null,
	precio decimal(11,2) not null,
	descuento decimal(11,2) not null,
	FOREIGN KEY (idVenta) REFERENCES venta (idVenta) ON DELETE CASCADE,
	FOREIGN KEY (idArticulo) REFERENCES articulo (idArticulo)
);




--Trigger para actualizar stock
--Stock aumenta porque se le compra a un proveedor
USE dbsistema;
GO
CREATE TRIGGER ActualizarStock_Ingreso
ON detalleIngreso
FOR INSERT
AS
UPDATE a SET a.stock = a.stock + d.cantidad
FROM articulo AS a INNER JOIN
INSERTED AS d ON d.idArticulo = a.idArticulo


--Trigger para actualizar stock
--Stock disminuye porque se le vende a un vendedor
USE dbsistema;
GO
CREATE TRIGGER ActualizarStock_Venta
ON detalleVenta
FOR INSERT
AS
UPDATE a SET a.stock = a.stock - d.cantidad
FROM articulo AS a INNER JOIN
INSERTED AS d ON d.idArticulo = a.idArticulo