﻿using Sistema.Entidades.Usuarios;
using Sistema.Entidades.Ventas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Sistema.Entidades.Almacen
{
    public class Ingreso
    {
        public int idIngreso { get; set; }
        [Required]
        public int idProveedor { get; set; }
        [Required]
        public int idUsuario { get; set; }
        [Required]
        public string tipoComprobante { get; set; }
        [Required]
        public string serieComprobante { get; set; }
        [Required]
        public string numComprobante { get; set; }
        [Required]
        public DateTime fechaHora { get; set; }
        [Required]
        public decimal impuesto { get; set; }
        [Required]
        public decimal total { get; set; }
        [Required]
        public string estado { get; set; }
        public ICollection<DetalleIngreso> detalles { get; set; }
        public Usuario usuario { get; set; }
        public Persona persona { get; set; }

    }
}
